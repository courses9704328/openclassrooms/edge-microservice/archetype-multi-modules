package $package;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ${artifactIdCamelCase}ApplicationTests {

	@Test
	void contextLoads() {
	}

}
